import { Component } from '@angular/core';

@Component({
    selector: 'post-item',
    templateUrl: 'partials/post-item.html',
    inputs: ['post']
})

export class PostItemComponent{}