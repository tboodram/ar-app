import { Component } from '@angular/core';
import { PostItemComponent } from './component.post-item'
import {Observable} from 'rxjs/Rx';  
import 'rxjs/add/operator/map'
import {Http} from '@angular/http';
import {ImageReddit} from './reddit_feed.model';


@Component({
  selector: 'reddit-feed',
  templateUrl: "app/reddit-feed.html",  
  //styleUrls: ['app/components/reddit_feed.css'],
  providers: []
})

export class AppComponent  { 

  currentPost = "";
  modifiedUrl = "";

  showPost(reddit){
    this.currentPost = reddit;
  }
  switchFeedToElephants(){
    this._redditDataUrl = 'http://www.reddit.com/r/babyelephantgifs.json';
    this._initFeed();
  }
  private _reddits$: Observable<Array<ImageReddit>>;
  private _redditDataUrl: string = 'http://www.reddit.com/r/aww.json';

  constructor(private _http: Http) {
  }

  ngOnInit() {
    this._initFeed();
    this._logFeed();
  }

  private _initFeed() {
    this._reddits$ = this._http.get(this._redditDataUrl)
      .map(response => response.json())
      .map(json => <Array<any>>json.data.children)
      /*
      .map(children => children.filter(d => (
        ['png', 'jpg'].indexOf(d.data.url.split('.').pop()) != -1
      )))
      */
      .map(children => children.map(d => new ImageReddit(d.data.id, d.data.title, d.data.url, d.data.domain, d.data.gilded)));
  }
  private _logFeed() {
    this._reddits$.subscribe(data => console.debug('data', data));
  }


}
