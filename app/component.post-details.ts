import { Component } from '@angular/core';

@Component({
    selector: 'post-details',
    templateUrl: 'partials/post-details.html',
    inputs: ['post']
})

export class PostDetailsComponent {

    gifMp4Link = "";
    gifWebmLink = "";
    gifThumbLink = "";
    imageLink = "";

    checkForJpeg(post) {
        if (post.indexOf(".jpg") != -1 || post.indexOf("reddituploads")!= -1 || post.indexOf("imgur")!= -1 && post.indexOf(".gif") == -1) {
            this.imageLink = String(post).replace(/amp;/g, "");
            if(post.indexOf("imgur")!= -1 && post.indexOf(".jpg") == -1){
                this.imageLink = String(post) + ".jpg";
            }
            console.log(post)
            console.log(this.imageLink);
            return true;
        } else {
            return false;
        }
    }

    checkForGif(post) {
        if (post.indexOf(".gif") != -1) {
            this.makeGifImgurLink(post)
            return true;
        } else {
            return false;
        }
    }

    makeGifImgurLink(post) {
        if (post.indexOf("imgur") != -1) {
            this.gifMp4Link = post.replace(".gifv", ".mp4");
            this.gifMp4Link = post.replace(".gif", ".mp4");

            this.gifWebmLink = post.replace(".gifv", ".webm");
            this.gifWebmLink = post.replace(".gif", ".webm");

            this.gifThumbLink = post.replace(".gifv", ".jpg");
            this.gifThumbLink = post.replace(".gif", ".jpg");
        }else if (post.indexOf("gfycat") != -1) {

        }
    }
    makeRedditUploadsLink(post) {
        if (post.indexOf("reddituploads") != -1) {
            this.imageLink = post.replace("&amp;", "&");
        }
    }
}