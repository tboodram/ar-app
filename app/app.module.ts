import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent }  from './app.component';
import { PostItemComponent }  from './component.post-item';
import { PostDetailsComponent }  from './component.post-details';
import { HttpModule } from '@angular/http';

@NgModule({
  imports:      [ BrowserModule, HttpModule ],
  declarations: [ AppComponent, PostItemComponent, PostDetailsComponent ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
