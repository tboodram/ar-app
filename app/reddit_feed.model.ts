export class ImageReddit {
  constructor(
    public id: any,
    public title: string,
    public mediaUrl: any,
    public domain: string,
    public gilded: boolean
  ) {
  }
}