"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
require('rxjs/add/operator/map');
var http_1 = require('@angular/http');
var reddit_feed_model_1 = require('./reddit_feed.model');
var AppComponent = (function () {
    function AppComponent(_http) {
        this._http = _http;
        this.currentPost = "";
        this.modifiedUrl = "";
        this._redditDataUrl = 'http://www.reddit.com/r/aww.json';
    }
    AppComponent.prototype.showPost = function (reddit) {
        this.currentPost = reddit;
    };
    AppComponent.prototype.switchFeedToElephants = function () {
        this._redditDataUrl = 'http://www.reddit.com/r/babyelephantgifs.json';
        this._initFeed();
    };
    AppComponent.prototype.ngOnInit = function () {
        this._initFeed();
        this._logFeed();
    };
    AppComponent.prototype._initFeed = function () {
        this._reddits$ = this._http.get(this._redditDataUrl)
            .map(function (response) { return response.json(); })
            .map(function (json) { return json.data.children; })
            .map(function (children) { return children.map(function (d) { return new reddit_feed_model_1.ImageReddit(d.data.id, d.data.title, d.data.url, d.data.domain, d.data.gilded); }); });
    };
    AppComponent.prototype._logFeed = function () {
        this._reddits$.subscribe(function (data) { return console.debug('data', data); });
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: 'reddit-feed',
            templateUrl: "app/reddit-feed.html",
            //styleUrls: ['app/components/reddit_feed.css'],
            providers: []
        }), 
        __metadata('design:paramtypes', [http_1.Http])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map