"use strict";
var ImageReddit = (function () {
    function ImageReddit(id, title, mediaUrl, domain, gilded) {
        this.id = id;
        this.title = title;
        this.mediaUrl = mediaUrl;
        this.domain = domain;
        this.gilded = gilded;
    }
    return ImageReddit;
}());
exports.ImageReddit = ImageReddit;
//# sourceMappingURL=reddit_feed.model.js.map