"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var PostDetailsComponent = (function () {
    function PostDetailsComponent() {
        this.gifMp4Link = "";
        this.gifWebmLink = "";
        this.gifThumbLink = "";
        this.imageLink = "";
    }
    PostDetailsComponent.prototype.checkForJpeg = function (post) {
        if (post.indexOf(".jpg") != -1 || post.indexOf("reddituploads") != -1 || post.indexOf("imgur") != -1 && post.indexOf(".gif") == -1) {
            this.imageLink = String(post).replace(/amp;/g, "");
            if (post.indexOf("imgur") != -1 && post.indexOf(".jpg") == -1) {
                this.imageLink = String(post) + ".jpg";
            }
            console.log(post);
            console.log(this.imageLink);
            return true;
        }
        else {
            return false;
        }
    };
    PostDetailsComponent.prototype.checkForGif = function (post) {
        if (post.indexOf(".gif") != -1) {
            this.makeGifImgurLink(post);
            return true;
        }
        else {
            return false;
        }
    };
    PostDetailsComponent.prototype.makeGifImgurLink = function (post) {
        if (post.indexOf("imgur") != -1) {
            this.gifMp4Link = post.replace(".gifv", ".mp4");
            this.gifMp4Link = post.replace(".gif", ".mp4");
            this.gifWebmLink = post.replace(".gifv", ".webm");
            this.gifWebmLink = post.replace(".gif", ".webm");
            this.gifThumbLink = post.replace(".gifv", ".jpg");
            this.gifThumbLink = post.replace(".gif", ".jpg");
        }
        else if (post.indexOf("gfycat") != -1) {
        }
    };
    PostDetailsComponent.prototype.makeRedditUploadsLink = function (post) {
        if (post.indexOf("reddituploads") != -1) {
            this.imageLink = post.replace("&amp;", "&");
        }
    };
    PostDetailsComponent = __decorate([
        core_1.Component({
            selector: 'post-details',
            templateUrl: 'partials/post-details.html',
            inputs: ['post']
        }), 
        __metadata('design:paramtypes', [])
    ], PostDetailsComponent);
    return PostDetailsComponent;
}());
exports.PostDetailsComponent = PostDetailsComponent;
//# sourceMappingURL=component.post-details.js.map